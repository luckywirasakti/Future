*******************
Future
*******************

Website ini berupa website jual beli furniture, seperti : mebel, maupun peralatan rumah tangga pada umumnya. Dalam website ini berkonsep seperti e-commerce namun dengan versi yang lebih mudah, fokus kami dalam pembuatan situs ini adalah mendekatkan antara produsen dan konsumen akhir, sehingga diharapkan dapat mendapatkan harga yang pas dan pantas di setiap transaksinya.

*******************
  Informasi Rilis
*******************
 - base css
 - image temporary
 - etc

*******************
  Pembagian Tugas
*******************
 - Pramuji Septiawan
   - Profile
   - Favorite
   - Lacak pesanan
   - Kebijakan & privasi
   - Panduan
 - Galih Adhi Kuncoro Rosyad
   - About
   - Promo
   - Kategori
   - Home
   - Halaman error
 - Lucky Adhikrisna Wirasakti
   - Blog
   - Testimonial
   - Etalase
   - Invoice
   - Formulir penjualan
 - Rony Permadi
   - Customer service
   - Login,register,forgot password
   - Single produk
   - Keranjang
   - Maintenance
*******************
  Kontributor
*******************
 - Pramuji Septiawan
 - Galih Adhi Kuncoro Rosyad
 - Lucky Adhikrisna Wirasakti
 - Rony Permadi
